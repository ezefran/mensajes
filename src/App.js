import { useEffect, useState } from 'react'
import Chat from './components/chat'
import Lateral from './components/lateral'
import styled from 'styled-components'
import './App.css'
import ChatsRepo from './repos/testRepo'

const Container = styled.div`
  height: 100%;

  display: flex;

  font-family: 'Lato', sans-serif;
`

const ChatVacio = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 100%;
  width: 100%;

  font-weight: bold;
  font-size: 50px;
  text-transform: uppercase;
  color: rgba(170, 170, 170, 0.4);
  border-left: 1px solid rgba(0,0,0,0.08);
`

function App() {
  const [chatSeleccionado, setChatSeleccionado] = useState(-1)
  const [chats, setChats] = useState([])
  const [recargarChats, setRecargarChats] = useState(false)
  const [mostrarLateral, setMostrarLateral] = useState(true)

  const contactos =
    chats && chats.length > 0 ? chats.map((chat) => chat.nombre) : []

  useEffect(() => {
    const _buscarChats = async () => {
      const chatsEnRepo = await ChatsRepo.buscarChats()
      setChats(chatsEnRepo)
      setRecargarChats(false)
    }
    _buscarChats()
  }, [recargarChats])

  useEffect(() => {
    if (window.screen.width < 550 && chatSeleccionado >= 0) {
      setMostrarLateral(false)
      window.history.pushState({ page: 'chat' }, null, '#chat')
    }
  }, [chatSeleccionado])

  useEffect(() => {
    if (window.screen.width < 550) {
      const handlePopstate = () => {
        setChatSeleccionado(-1)
        setMostrarLateral(true)
      }
      window.addEventListener('popstate', handlePopstate)

      return () => {
        window.removeEventListener('popstate', handlePopstate)
      }
    }
  }, [])

  return (
    <Container>
      {mostrarLateral ? (
        <Lateral
          contactos={contactos}
          chatSeleccionado={chatSeleccionado}
          setChatSeleccionado={setChatSeleccionado}
          setRecargarChats={setRecargarChats}
        />
      ) : null}
      {chatSeleccionado >= 0 ? (
        <Chat
          nombre={chats[chatSeleccionado].nombre}
          mensajes={chats[chatSeleccionado].mensajes}
        />
      ) : (
        <ChatVacio>
          <p>Seleccionar chat</p>
        </ChatVacio>
      )}
    </Container>
  )
}

export default App
