import styled from 'styled-components'

const Container = styled.article`
  display: flex;
  flex-direction: column;
  gap: 10px;
`

const NombreContainer = styled.p`
  font-size: 20px;
  color: rgba(0,0,0,0.733);
  font-weight: bold;
`

const MsgContainer = styled.section`
  padding: 20px 25px;

  border-radius: 17px;
  background-color: #ADECA8;
  box-shadow: 2px 2px 8px 0px rgba(0,0,0,0.05);

  & > p {
    font-family: Arial, sans-serif;
    font-size: 20px;
    color: rgba(0,0,0,0.733);

    cursor: text;
    user-select: text;
  }

  @media (max-width: 550px) {
    padding: 15px 17px;
  }
`

function Mensaje({ nombre, contenido }) {
  return (
    <Container>
      <NombreContainer>{nombre}</NombreContainer>
      <MsgContainer>
        <p>{contenido}</p>
      </MsgContainer>
    </Container>
  )
}

export default Mensaje
