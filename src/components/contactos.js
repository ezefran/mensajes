import styled from 'styled-components'

const Container = styled.section`
  flex-grow: 1;

  display: flex;
  flex-direction: column;

  overflow-y: auto;
  background-color: #fff;

  ::-webkit-scrollbar {
    width: 7px;
  }

  ::-webkit-scrollbar-track {
    background-color: transparent;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #21CF74;
  }
`

const Contacto = styled.article`
  min-height: 70px;
  padding-left: 20px;

  display: flex;
  align-items: center;

  background-color: ${(props) => (props.activo ? '#47C573' : 'rgb(255,255,255)')};
  color: ${(props) => (props.activo ? '#fff' : 'rgba(0,0,0,0.72)')};
  font-size: 20px;
  font-weight: bold;
  box-shadow: 2px 2px 8px 0px rgba(0,0,0,0.13);

  cursor: pointer;
  & > * {
    cursor: pointer;
  }

  &:hover {
    background-color: rgba(71, 197, 115, 0.7);
    color: #fff;
  }
`

function Contactos({ contactos, chatSeleccionado, setChatSeleccionado }) {
  const contactosElement = contactos.map((contacto, index) => {
    return (
      <Contacto
        activo={chatSeleccionado === index}
        id={index}
        key={index}
        onClick={() => setChatSeleccionado(index)}
      >
        <p>{contacto}</p>
      </Contacto>
    )
  })

  return <Container>{contactosElement}</Container>
}

export default Contactos
