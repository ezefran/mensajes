import Mensaje from './mensaje'
import styled from 'styled-components'

const Container = styled.section`
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;

  @media (max-width: 550px) {
    max-width: 100vw;
  }
`

const Header = styled.header`
  min-height: 70px;
  padding-left: 32px;

  display: flex;
  align-items: center;

  background-color: #36a48a;
  font-size: 24px;
  color: rgba(255,255,255,0.88);
  font-weight: bold;
  border-left: 1px solid rgba(255,255,255,0.1);
`

const Contenido = styled.section`
  flex-grow: 1;
  padding: 20px 32px;

  display: flex;
  flex-direction: column;
  gap: 20px;

  overflow-y: auto;
  border-left: 1px solid rgba(0,0,0,0.08);

  ::-webkit-scrollbar {
    width: 7px;
  }

  ::-webkit-scrollbar-track {
    background-color: transparent;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #21CF74;
  }

  @media (max-width: 550px) {
    gap: 6px;
  }
`

function Chat({ nombre, mensajes }) {
  const mensajesElement = mensajes
    ? mensajes.map((mensaje, index) => (
        <Mensaje
          key={index}
          nombre={mensaje.nombre}
          contenido={mensaje.contenido}
        />
      ))
    : null

  return (
    <Container>
      <Header>
        <p>{nombre}</p>
      </Header>
      <Contenido>{mensajesElement}</Contenido>
    </Container>
  )
}

export default Chat
