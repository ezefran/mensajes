import Contactos from './contactos'
import styled from 'styled-components'
import { FaSyncAlt } from 'react-icons/fa'
import { useState } from 'react'

const Container = styled.section`
  min-width: 300px;

  display: flex;
  flex-direction: column;

  background: linear-gradient(#36a48a, #48c673);

  @media (max-width: 550px) {
    min-width: 100vw;
  }
`

const Header = styled.header`
  min-height: 70px;

  display: flex;
  justify-content: center;
  align-items: center;

  font-size: 26px;
  font-weight: bold;
  text-transform: uppercase;
  color: rgba(255,255,255,0.88);
`

const Footer = styled.footer`
  min-height: 80px;

  display: flex;
  justify-content: center;
  align-items: center;
`

const Boton = styled.button`
  height: 60px;
  width: 60px;

  background-color: rgba(64,64,64,0.125);
  border: none;
  border-radius: 100%;
  outline: none;

  cursor: pointer;
  * {
    cursor: pointer;
    color: rgba(255,255,255,0.88);
    font-size: 30px;
  }

  &:hover {
    background-color: rgba(64,64,64,0.25);
  }

  &:disabled * {
    color: rgba(255,255,255,0.333);
  }
`

function Lateral({
  contactos,
  chatSeleccionado,
  setChatSeleccionado,
  setRecargarChats,
}) {
  const [botonDesactivado, setBotonDesactivado] = useState(false)

  const onClickRecargar = () => {
    setRecargarChats(true)
    setBotonDesactivado(true)

    setTimeout(() => {
      setBotonDesactivado(false)
    }, 5000)
  }

  return (
    <Container>
      <Header>
        <p>Mensajes</p>
      </Header>
      <Contactos
        contactos={contactos}
        chatSeleccionado={chatSeleccionado}
        setChatSeleccionado={setChatSeleccionado}
      />
      <Footer>
        <Boton disabled={botonDesactivado} onClick={() => onClickRecargar()}>
          <FaSyncAlt />
        </Boton>
      </Footer>
    </Container>
  )
}

export default Lateral
