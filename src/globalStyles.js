import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    cursor: default;
    user-select: none;
  }
  
  html, body, #root {
    margin: 0;
    height: 100%;
    overflow: hidden;
  }

  body {
    display: flex;
    flex-direction: column;
  }

  #root {
    flex-grow: 1;
  }

  p {
    margin: 0;
  }

  @media (max-width: 550px) {
    *:active {
      -webkit-tap-highlight-color: transparent;
    }
  }
`

export default GlobalStyle
