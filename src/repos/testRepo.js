const chats = [
  {
    nombre: 'Pepe',
    mensajes: [
      {
        nombre: 'Pepe',
        contenido: 'Hola Jorge!!',
      },
      {
        nombre: 'Jorge',
        contenido: 'Hola!!',
      },
      {
        nombre: 'Pepe',
        contenido: 'Como va?!',
      },
      {
        nombre: 'Jorge',
        contenido: 'Bien!',
      },
      {
        nombre: 'Pepe',
        contenido: ':D',
      },
    ],
  },
  {
    nombre: 'Pedro',
    mensajes: [
      {
        nombre: 'Pedro',
        contenido: 'Hola Jorgee',
      },
    ],
  },
  { nombre: 'Luis' },
  { nombre: 'Pedro 2' },
  { nombre: 'Jorge 2' },
  { nombre: 'Luis 2' },
  { nombre: 'Pepe 2' },
  { nombre: 'Pedro 3' },
  { nombre: 'Jorge 3' },
  { nombre: 'Luis 3' },
  { nombre: 'Pepe 3' },
]

const ChatsRepo = () => {
  const buscarChats = async () => {
    return chats
  }

  return {
    buscarChats,
  }
}

export default ChatsRepo()
