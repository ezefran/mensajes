const { app, BrowserWindow } = require('electron')

function createWindow() {
  const win = new BrowserWindow({
    minWidth: 900,
    minHeight: 650,
    show: false,
    frame: false,
    webPreferences: {
      enableRemoteModule: true,
      nodeIntegration: true,
    },
  })

  win.loadFile('build/index.html')
  win.removeMenu()

  win.once('ready-to-show', () => {
    win.show()
  })
}

app.whenReady().then(createWindow)
